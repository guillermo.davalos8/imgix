# imgix

## Frontend Engineer Assignment for candidate Guillermo Davalos Lopez

- Full Stack Developer with over ten years of experience. I have had the chance to help clientsaccomplish their digital transformation goals and deliver solutions across several industries such asAutomotive, Manufacturing, Insurance, Retail, Real Estate, Fitness and Finance.

## Technologies Used:

- [React](https://reactjs.org/) for the frontend, application was bootstrapped with [create-react-app](https://reactjs.org/docs/create-a-new-react-app.html#create-react-app). Some other packages of note:
  - [react-router-dom](https://reactrouter.com/web/guides/quick-start) for navigation.
  - [sass](https://sass-lang.com/) for CSS preprocessing.
  - [classnames](https://www.npmjs.com/package/classnames) for consistent class concatenation.
  - [qs](https://www.npmjs.com/package/qs) for query string processing.

- [Node](https://nodejs.org/en/) for the backend, in this particular application only used as a very basic web server to serve the SPA application's static files. Some packages of note:
  - [express](https://expressjs.com/) web framework for node, never made a web application that uses node without it.
  - [helmet](https://helmetjs.github.io/) provides various security HTTP headers automatically and also for CSP management.
  - [dotenv](https://www.npmjs.com/package/dotenv) for environment variable management.

- [Google Cloud Platform (GCP)](https://cloud.google.com/gcp) for hosting the application, services used in particular:
  - [Cloud Run](https://cloud.google.com/run) since the application is containerized using Docker I went with serverless on Cloud Run for availability and scalability.

- [Docker](https://www.docker.com/) for containerization of the application, platform independence and scalability.

## Location:
- The app can be found at https://gdavaloslopez.com
- Repository can be found at https://gitlab.com/guillermo.davalos8/imgix

