import React, { Component } from 'react';

const GlobalContext = React.createContext();

export class GlobalProvider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sideDrawer: false,
      setSideDrawer: (sideDrawer, callback) => { this.setState({ sideDrawer }, callback) },
    };
  }

  render() {
    const { children } = this.props;
    return (
        <GlobalContext.Provider value={ this.state }>
          { children }
        </GlobalContext.Provider>
    );
  }
}

export const GlobalConsumer = GlobalContext.Consumer;

export default GlobalContext;