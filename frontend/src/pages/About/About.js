import './About.scss';
import React from 'react';
import c from 'classnames';

const BLOCK = 'about';

class About extends React.Component {
  constructor(props) {
    super(props);

    this.state = {

    };
  }

  render() {
    return (
      <div className={c(BLOCK)}>
        <div className='container'>
          <h1>About</h1>
          <div className={c(`${BLOCK}__card`)}>
            <p className='emphasis'>
              Candidate:
            </p>
            <p>
              Guillermo Davalos Lopez
            </p>
            <p className='emphasis'>
              Email:
            </p>
            <p>
              guillermo.davalos8@gmail.com
            </p>
            <p className='emphasis'>
              Technologies Used:
            </p>
            <p>
              React (frontend), Node (Backend), GCP (Cloud Computing), Docker (Container)
            </p>
          </div>
        </div>
      </div>     
    );
  }
}

export default About;
