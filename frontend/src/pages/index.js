import LayoutI from './Layout/Layout';
import GeneratorI from './Generator/Generator';
import AboutI from './About/About';

export const Layout = LayoutI;
export const Generator = GeneratorI;
export const About = AboutI;
