import './Generator.scss';
import React from 'react';
import c from 'classnames';
import { Button, Dropdown } from '../../components';
import { generateSrc } from '../../utils';

const BLOCK = 'generator';
const DEFAULT_STATE = {
  text: '',
  hexCode: '#000000',
  src: '',
  hexCodeFormatted: '000000',
  imageTemplate: '/examples/butterfly.jpg'
};

class Generator extends React.Component {
  constructor(props) {
    super(props);

    this.state = DEFAULT_STATE;

    this.formRef = React.createRef();
    this.preSave = this.preSave.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  preSave(e) {
    e.preventDefault();

    this.formRef.current.checkValidity();
    if(!this.formRef.current.reportValidity()) {
      return;            
    };

    this.handleSubmit();
  }

  handleSubmit() {
    const { text, hexCode, imageTemplate } = this.state;
    this.setState({
      src: generateSrc(text, hexCode, imageTemplate)
    });
  }

  render() {
    return (
      <div className={c(BLOCK)}>
        <div className='container'>
          <div className={c(`${BLOCK}__header-section`)}>
            <h1>Welcome to the imgix generator!</h1>
          </div>    
          <div className={c(`${BLOCK}__card`)}>        
            <div className={c(`${BLOCK}__options`)}>
              <div className={c(`${BLOCK}__clear-section`)}>
                <span className='emphasis'>Options</span>
                <span className={c(`${BLOCK}__clear`)}
                  onClick={() => this.setState(DEFAULT_STATE)}
                >Clear All</span>
              </div>
              <form className={c(`${BLOCK}__input-section`)} ref={this.formRef}>
                <div className={c(`${BLOCK}__inputs`)}>
                  <div className={c(`${BLOCK}__input-group`)}>
                    <p className='emphasis'>Text</p>
                    <input 
                      required
                      type='text' placeholder='Type Away!'
                      onChange={(e) => this.setState({ text: e.target.value })}
                      value={this.state.text}
                    />
                  </div>
                  <div className={c(`${BLOCK}__input-group`)}>
                    <p className='emphasis'>Blend Color</p>
                    <input 
                      required
                      type='color' placeholder='Hex Code'
                      onChange={(e) => this.setState({ hexCode: e.target.value })}
                      value={this.state.hexCode}
                    />
                  </div>
                  <div className={c(`${BLOCK}__input-group`)}>
                    <p className='emphasis'>Image Template</p>
                    <Dropdown 
                      disabled={false} 
                      onChangeHandler={(e) => {
                        this.setState({ imageTemplate: e.target.value })
                      }}
                      value={this.state.imageTemplate}
                      options={[
                        { value: '/examples/butterfly.jpg', label: 'Butterfly'},
                        { value: '/unsplash/bridge.jpg', label: 'Bridge'},
                        { value: '/unsplash/pineneedles.jpg', label: 'Pine Needles'},
                        { value: '/unsplash/motorbike.jpg', label: 'Motorbike'},
                        { value: '/unsplash/mountains.jpg', label: 'Mountains'},                                    
                      ]}
                    />
                  </div>
                </div>
                <div className={c(`${BLOCK}__input-btns`)}>
                  <Button
                    text='Generate!'
                    type='submit'
                    extraClasses={['button__primary']}
                    onClickHandler={this.preSave}
                  />
                </div>
              </form>
            </div>
          </div>
          {
            this.state.src ? 
              <div className={c(`${BLOCK}__card`, `${BLOCK}__img-card`)}>
                <div className={c(`${BLOCK}__img`)}>
                  <div className={c(`${BLOCK}__img-container`)}>
                    <img 
                      src={this.state.src}
                      alt='imgix generator'
                    />
                  </div>
                </div>
              </div>
            : null
          }
        </div>
      </div>     
    );
  }
}

export default Generator;
