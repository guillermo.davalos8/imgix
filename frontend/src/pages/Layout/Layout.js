import './Layout.scss';
import React from 'react';
import GlobalContext from '../../contexts/GlobalContext';
import { Header, SideDrawer } from '../../components';
import c from 'classnames';
import { Switch, Route } from 'react-router-dom';
import { About, Generator } from '..';

const BLOCK = 'layout';

class Layout extends React.Component {
  render() {
    return (
      <div className={c(BLOCK)}>        
        <Header />
        <SideDrawer />
        <div className={c(`${BLOCK}__content`)}> 
          <Switch>
            <Route key='0' exact path='/' component={Generator} />
            <Route key='1' exact path='/generator' component={Generator} />
            <Route key='3' exact path='/about' component={About} />
          </Switch>        
        </div>        
      </div>
    );
  }
}

Layout.contextType = GlobalContext;

export default Layout;