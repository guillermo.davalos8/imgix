import './Header.scss';
import React, { useContext } from 'react';
import c from 'classnames';
import { ReactComponent as Menu } from '../../assets/menu.svg';
import { ReactComponent as Close } from '../../assets/close.svg';
import GlobalContext from '../../contexts/GlobalContext';
import { ReactComponent as ImgixLogo } from '../../assets/imgix-logo.svg';

const BLOCK = 'header';

const Header = () => {  
  const { sideDrawer, setSideDrawer } = useContext(GlobalContext); 

  return(
    <div className={c(`${BLOCK}`)}>
      <div className={c(`${BLOCK}__menu`)}>
        <div className={c(`${BLOCK}__menu-btn`)} onClick={() => setSideDrawer(!sideDrawer)}>
          {
            sideDrawer ? 
            <Close /> :
            <Menu />
          }
        </div>
      </div>
      <div className='container'>
        <div className={c(`${BLOCK}__logo`)}>
          <ImgixLogo />
        </div>
      </div>
    </div>
  );
};

export default Header;