import './Button.scss';
import React from 'react';
import c from 'classnames';

const BLOCK = 'button';

const Button = ({ text, extraClasses = [], onClickHandler, type }) => {  
  return(
    <button className={c(BLOCK, ...extraClasses)} onClick={onClickHandler} type={type || 'button'}>      
      { text }
    </button>
  );
};

export default Button;