import './SideDrawer.scss';
import React from 'react';
import c from 'classnames';
import GlobalContext from '../../contexts/GlobalContext';
import { withRouter } from 'react-router';
import { ReactComponent as ChevronRight } from '../../assets/chevron-right.svg';

const BLOCK = 'side-drawer';

class SideDrawer extends React.Component {
  render() {
    return(
      <div className={c(BLOCK, this.context.sideDrawer ? `${BLOCK}__show` : `${BLOCK}__hidden`)}>
        <div 
          className={c(`${BLOCK}__element`)}
          onClick={() => { this.context.setSideDrawer(false); this.props.history.push('/generator')} }
        >
          <span
            className={c(`${BLOCK}__element-txt`)}
          > Generator <ChevronRight />
          </span>
        </div>
        <div 
          className={c(`${BLOCK}__element`)}
          onClick={() => { this.context.setSideDrawer(false); this.props.history.push('/about')} }
        >
          <span
            className={c(`${BLOCK}__element-txt`)}
          > About <ChevronRight />
          </span>
        </div>
      </div>
    );
  }
}

SideDrawer.contextType = GlobalContext;

export default withRouter(SideDrawer);