import './Dropdown.scss';
import React from 'react';
import c from 'classnames';
import { ReactComponent as Unfold } from '../../assets/unfold.svg';

const BLOCK = 'dropdown';

const Dropdown = ({ disabled, onChangeHandler, value, options, extraClasses, icon}) => {  
  return(
    <div className={c(`${BLOCK}`, extraClasses && extraClasses.length ? [...extraClasses] : null)}>      
      <select
        className={c(`${BLOCK}__select`)}
        disabled={disabled}
        onChange={onChangeHandler}
        value={value}
      >
        {
          options.map((o, i) => <option key={i} value={o.value}>{o.label}</option>)
        }
      </select>
      <span className={c(`${BLOCK}__arrow`)}>{icon ? icon : <Unfold />}</span>
    </div>
  );
};

export default Dropdown;