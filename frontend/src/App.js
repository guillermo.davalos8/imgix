import './App.scss';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { GlobalProvider } from './contexts/GlobalContext';
import { Layout } from './pages';
import React from 'react';

class App extends React.Component {
  render() {
    return (
      <GlobalProvider>
        <BrowserRouter>
          <Switch>
            <Route path='/' component={Layout} />
          </Switch>
        </BrowserRouter> 
      </GlobalProvider> 
    );    
  }
}

export default App;
