import qs from 'qs';

const BASE_URL = 'https://assets.imgix.net';
const DEFAULT_OPTIONS = {
  w: '640',
  txtclr: 'fff',
  txtalign: 'center%2Cmiddle',
  txtsize: '48',
  bm: 'normal',
  balph: '50'
};

export const generateSrc = (text, hexCode, imageTemplate) => {
  const hexCodeFormatted = hexCode.replace('#','');
  return `${BASE_URL}${imageTemplate}?${qs.stringify({ txt: text, blend: hexCodeFormatted, ...DEFAULT_OPTIONS })}`;    
}
