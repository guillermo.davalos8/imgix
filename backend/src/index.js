import express from 'express';
import path from 'path';
import helmet from 'helmet';
import { 
  NODE_ENV, PORT
} from './config/index.js';

const app = express();
const __dirname = path.resolve();
app.use(
  helmet({
    contentSecurityPolicy: {
      useDefaults: true,
      directives: {
        'img-src': ["'self'", 'data: https:'],
      },
    }
  })
);
app.use(express.static(path.join(__dirname,'public')));

app.use('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

app.listen(PORT, () => {
  console.log(`
    ------------------------------------
    Server Started
    NODE_ENV: ${NODE_ENV} typeof ${typeof NODE_ENV}
    PORT: ${PORT} typeof ${typeof PORT}
    ------------------------------------
  `);
});

app.use(function(err, req, res, next) {
  console.log(err);
  return res.status(err.status || 500).json({
    message: 'Internal Server Error'
  });
});
