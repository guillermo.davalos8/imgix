FROM node

WORKDIR /tmp/imgix
COPY . .
RUN rm -rf backend/public \
    && cd frontend \ 
    && npm install \
    && npm run build:prod \
    && mv build/ ../backend/public/    

WORKDIR /tmp/imgix/backend
RUN npm install 

WORKDIR /opt/imgix
RUN mv /tmp/imgix/backend/ . \
    && rm -rf /tmp/imgix

WORKDIR /opt/imgix/backend

CMD ["npm", "run", "start:prod"]